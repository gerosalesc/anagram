# Anagrams and ananagrams
## Version 0.1

Take a dictionary file and find anagrams groups or ananagrams(aka words without anagrams in the file) if specified
by the argument. This command require **Python 2.7+** to run.

Use:

```
main.py [OPTIONS]
OPTIONS:
-h              Show help
-a              Prints ananagrams
--path PATH     Path to the file containing the file, default path is the working directory
```