__author__ = 'chamanx'
# encoding: utf-8

import sys
import getopt
import logging
import logging.config
import time
import math
import datetime
import string
import os
import re

HELP_FILE = '/README.md'


def get_file_lines(path):
    """Get file lines yielded

    Args:
        path: path to file

    Return:
        response: Vector of stripped file lines
    """
    with open(path, 'r') as infile:
        for line in infile:
            yield line.strip()


def print_file_content(path):
    """print all file content with end lines characters

    Args:
        path: path to file

    Return:
    """
    lines = get_file_lines(path)
    out_text = ''
    for line in lines:
        out_text += line + '\n'
    print out_text


def invalid_arguments(cod):
    print_file_content(os.getcwd() + HELP_FILE)
    sys.exit(cod)


def strip_quotes(s):
    if s.endswith("\""):
        s = s[:-1]
    if s.startswith("\""):
        s = s[1:]
    return s


def main(argv):
    # arguments defaults
    path = 'wl.dic'
    ananagram = False

    # Set the logging parameters
    # formatx = '%(message)s'
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('profileLogger')
    logger.info('--------------Init started');

    # get from arguments vector
    try:
        opts, args = getopt.getopt(argv, "a", ['path=', ])

        for opt, arg in opts:
            arg = arg.strip()
            if opt == '-h':
                sys.exit(0)
            elif opt == '-a':
                ananagram = True
            elif opt == '--path':
                path = strip_quotes(arg)
    except getopt.GetoptError:
        invalid_arguments(2)
    except ValueError:
        invalid_arguments(2)
    except Exception:
        logger.error(sys.exc_info()[0])
        invalid_arguments(2)

    # Mark initial datetime
    initial_date_time = datetime.datetime.now()

    anagram_analisis(path, ananagram, logger)

    # Mark initial datetime and calc total time
    final_date_time = datetime.datetime.now()
    duration = final_date_time - initial_date_time
    logger.info('Program lasted: %s days, %s seconds, %s microseconds', duration.days, duration.seconds,
                duration.microseconds)

    logger.info('--------------Program finished');


def chars_alphabetic_sorting(word):
    """
    Sorts character of a word alphabetically
    """
    chars = list(word)
    chars.sort()
    return ''.join(chars)


def anagram_words_dictionary(word_list):
    """
    take each word of the list and apply alpha sort of its characters
    then save result in a dictionary
    """
    dictionary = {}
    for word in word_list:
        sorted_word = chars_alphabetic_sorting(word)
        if sorted_word in dictionary:
            anagram_group = dictionary[sorted_word]
            anagram_group.append(word)
            dictionary[sorted_word] = anagram_group
        else:
            anagram_group = [word]
            dictionary[sorted_word] = anagram_group
    return dictionary


def anagram_analisis(path, ananagram, logger):
    """
    Read dictionary printing anagrams and no anagrams grouping
    """
    lines = get_file_lines(path)
    dictionary = anagram_words_dictionary(lines)
    # Print anagrams group form dictionary
    for anagram_group in dictionary.itervalues():
        # Ananagrams
        if ananagram:
            if len(anagram_group) == 1:
                logger.info('%s', anagram_group[0])
        else:
            # We make sure is not a group
            if len(anagram_group) > 1:
                logger.info('%s', ', '.join(anagram_group))


if __name__ == '__main__':
    main(sys.argv[1:])
